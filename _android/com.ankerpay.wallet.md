---
title: "AnkerPay: Blockchain Crypto Wallet – BTC, ETH, LTC"
altTitle: 

users: 5000
appId: com.ankerpay.wallet
launchDate: 
latestUpdate: 2020-10-21
apkVersionName: "v1.0.11.26"
stars: 4.3
ratings: 87
reviews: 76
size: 4.9M
website: https://ankerid.com/mobile-wallet/
repository: 
issue: 
icon: com.ankerpay.wallet.png
bugbounty: 
verdict: nosource # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-06-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: AnkerPay
providerLinkedIn: 
providerFacebook: AnkerPlatform
providerReddit: 

redirect_from:
  - /com.ankerpay.wallet/
  - /posts/com.ankerpay.wallet/
---


This app promises to be non-custodial:

> AnkerSwitch is an alternative cryptocurrency exchange where users have more
  control. Swap any 2 currencies seamlessly while still controlling the assets
  within your wallet. This allows for complete control and safety since your
  crypto will never be left on an exchange.

Unfortunately neither the description on Google Play nor their website has a
link to source code which leads to our verdict **not verifiable**.
