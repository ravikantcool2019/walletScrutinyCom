---
title: "Huobi Global"
altTitle: 

users: 1000000
appId: pro.huobi
launchDate: 
latestUpdate: 2020-10-20
apkVersionName: "5.9.4"
stars: 3.9
ratings: 5281
reviews: 1834
size: 51M
website: https://www.huobi.com/en-us/
repository: 
issue: 
icon: pro.huobi.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: HuobiGlobal
providerLinkedIn: 
providerFacebook: huobiglobalofficial
providerReddit: 

redirect_from:
  - /pro.huobi/
  - /posts/pro.huobi/
---


Neither on Google Play nor on their website can we find a claim of a
non-custodial part to this app. We assume it is a purely custodial interface to
the exchange of same name and therefore **not verifiable**.