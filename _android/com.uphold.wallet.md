---
title: "Uphold: buy and sell Bitcoin"
altTitle: 

users: 1000000
appId: com.uphold.wallet
launchDate: 
latestUpdate: 2020-10-21
apkVersionName: "4.12.8"
stars: 3.4
ratings: 8333
reviews: 4542
size: 42M
website: https://uphold.com/
repository: 
issue: 
icon: com.uphold.wallet.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-10-12
reviewStale: true
signer: 
reviewArchive:


providerTwitter: UpholdInc
providerLinkedIn: company/upholdinc
providerFacebook: UpholdInc
providerReddit: 

redirect_from:
  - /com.uphold.wallet/
  - /posts/com.uphold.wallet/
---


This app appears to be an interface to a custodial trading platform. In the
Google Play description we read:

> Trust Through Transparency
> Uphold is fully reserved. Unlike banks, we don’t loan out your money. To prove
  it, we publish our holdings in real time.

If they hold your money, you don't. As a custodial service this app is **not
verifiable**.