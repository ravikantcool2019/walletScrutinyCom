---
title: "elegro Wallet: instant crypto-fiat payments"
altTitle: 

users: 500
appId: paydeepp.elegro.io
launchDate: 
latestUpdate: 2020-10-28
apkVersionName: "4.9.8"
stars: 4.2
ratings: 20
reviews: 8
size: 8.1M
website: 
repository: 
issue: 
icon: paydeepp.elegro.io.jpg
bugbounty: 
verdict: fewusers # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /paydeepp.elegro.io/
  - /posts/paydeepp.elegro.io/
---


This page was created by a script from the **appId** "paydeepp.elegro.io" and public
information found
[here](https://play.google.com/store/apps/details?id=paydeepp.elegro.io).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.