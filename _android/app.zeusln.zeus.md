---
title: "Zeus: Bitcoin/Lightning Wallet"
altTitle: 

users: 100
appId: app.zeusln.zeus
launchDate: 
latestUpdate: 2020-10-31
apkVersionName: "0.4.0"
stars: 4.7
ratings: 11
reviews: 4
size: 26M
website: 
repository: 
issue: 
icon: app.zeusln.zeus.png
bugbounty: 
verdict: fewusers # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-07-15
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /app.zeusln.zeus/
  - /posts/app.zeusln.zeus/
---


This app is the continuation of [this app](/com.zeusln.zeus/).
