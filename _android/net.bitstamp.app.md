---
title: "Bitstamp – Buy & Sell Bitcoin at Crypto Exchange"
altTitle: 

users: 100000
appId: net.bitstamp.app
launchDate: 
latestUpdate: 2020-10-19
apkVersionName: "1.5.6.1"
stars: 4.5
ratings: 6344
reviews: 1795
size: 12M
website: https://www.bitstamp.net/
repository: 
issue: 
icon: net.bitstamp.app.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: Bitstamp
providerLinkedIn: company/bitstamp
providerFacebook: Bitstamp
providerReddit: 

redirect_from:
  - /net.bitstamp.app/
  - /posts/net.bitstamp.app/
---


On the Google Play description we read:

> Convenient, but secure
>
> ● We store 98% of all crypto assets in cold storage

which means you don't get the keys for your coins. This is a custodial service
and therefore **not verifiable**.