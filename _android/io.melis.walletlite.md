---
title: "Melis Lite"
altTitle: 

users: 10
appId: io.melis.walletlite
launchDate: 2019-06-03
latestUpdate: 2019-06-03
apkVersionName: "0.4.0"
stars: 0.0
ratings: 
reviews: 
size: 16M
website: 
repository: 
issue: 
icon: io.melis.walletlite.png
bugbounty: 
verdict: fewusers # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-04-07
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /io.melis.walletlite/
  - /posts/io.melis.walletlite/
---


This page was created by a script from the **appId** "io.melis.walletlite" and public
information found
[here](https://play.google.com/store/apps/details?id=io.melis.walletlite).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.