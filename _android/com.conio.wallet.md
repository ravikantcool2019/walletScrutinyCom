---
title: "Conio Bitcoin Wallet"
altTitle: 

users: 10000
appId: com.conio.wallet
launchDate: 
latestUpdate: 2020-09-21
apkVersionName: "3.3.3"
stars: 3.7
ratings: 362
reviews: 240
size: 69M
website: https://www.conio.com/en/
repository: 
issue: 
icon: com.conio.wallet.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-04-09
reviewStale: true
signer: 
reviewArchive:


providerTwitter: conio
providerLinkedIn: company/conio
providerFacebook: ConioHQ
providerReddit: 

redirect_from:
  - /com.conio.wallet/
  - /posts/com.conio.wallet/
---


This app has wonderful security claims on Google Play:

> *5. STAY CAREFREE* With Conio you can recover your Bitcoins even if you forget
> all your credentials! Just create your profile and upload a selfie when you
> start.

Unfortunately this means that the provider does control the keys which makes it
a custodial app. Our verdict: **not verifiable**.
