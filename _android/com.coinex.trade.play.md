---
title: "CoinEx"
altTitle: 

users: 10000
appId: com.coinex.trade.play
launchDate: 
latestUpdate: 2020-09-22
apkVersionName: "1.9.1.1"
stars: 4.5
ratings: 1063
reviews: 502
size: 13M
website: https://www.coinex.co/
repository: 
issue: 
icon: com.coinex.trade.play.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-04-15
reviewStale: true
signer: 
reviewArchive:


providerTwitter: coinexcom
providerLinkedIn: 
providerFacebook: TheCoinEx
providerReddit: Coinex

redirect_from:
  - /com.coinex.trade.play/
  - /posts/com.coinex.trade.play/
---


The description on Google Play starts not so promising:

> Meet the top cryptocurrency trading app！

Trading apps are usually custodial. Unfortunately there is no easily accessible
information on their website about the app neither. For now we assume it is
custodial and thus **not verifiable**.