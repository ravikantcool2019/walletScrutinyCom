---
title: "Cryptonator cryptocurrency wallet"
altTitle: 

users: 100000
appId: com.cryptonator.android
launchDate: 2018-11-01
latestUpdate: 2020-01-17
apkVersionName: "3.0.1"
stars: 3.7
ratings: 4383
reviews: 2561
size: 8.6M
website: https://www.cryptonator.com/
repository: 
issue: 
icon: com.cryptonator.android.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-03-17
reviewStale: false
signer: 
reviewArchive:
- date: 2019-11-12
  version: "3.0.1"
  apkHash: 
  gitRevision: acb5634ce0405f12d9924759b045407fde297306
  verdict: nosource

providerTwitter: cryptonatorcom
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /cryptonator/
  - /com.cryptonator.android/
  - /posts/2019/11/cryptonator/
  - /posts/com.cryptonator.android/
---


Cryptonator cryptocurrency wallet
makes no claim to be non-custodial but the
[Customer Support](https://www.cryptonator.com/contact/other/)
is pretty unambigously pointing towards it being custodial:

> **Do you provide private keys?**: No

Absent source code this wallet is **not verifiable**.
