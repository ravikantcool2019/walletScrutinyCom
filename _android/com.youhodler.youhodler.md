---
title: "YouHodler - Crypto and Bitcoin Wallet"
altTitle: 

users: 100000
appId: com.youhodler.youhodler
launchDate: 
latestUpdate: 2020-08-27
apkVersionName: "2.8.0"
stars: 4.0
ratings: 443
reviews: 167
size: 59M
website: https://youhodler.com/
repository: 
issue: 
icon: com.youhodler.youhodler.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-06-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: youhodler
providerLinkedIn: company/youhodler
providerFacebook: YouHodler
providerReddit: 

redirect_from:
  - /com.youhodler.youhodler/
  - /posts/com.youhodler.youhodler/
---


This app is the interface to an exchange and might have a non-custodial part to
it but if so, it is not well advertised on their website and we assume it is
custodial and therefore **not verifiable**.
