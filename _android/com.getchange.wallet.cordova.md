---
title: "Change: Crypto Wallet to Buy Sell & Trade Bitcoin"
altTitle: 

users: 50000
appId: com.getchange.wallet.cordova
launchDate: 
latestUpdate: 2020-10-20
apkVersionName: "10.9.80"
stars: 4.3
ratings: 948
reviews: 480
size: 32M
website: https://getchange.com
repository: 
issue: 
icon: com.getchange.wallet.cordova.jpg
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: changefinance
providerLinkedIn: company/changeinvest
providerFacebook: changeinvest
providerReddit: 

redirect_from:
  - /com.getchange.wallet.cordova/
  - /posts/com.getchange.wallet.cordova/
---


On their Google Play description we find

> • Secure: Funds are protected in multi-signature, cold-storage cryptocurrency
  wallets

which means it is a custodial service and thus **not verifiable**.