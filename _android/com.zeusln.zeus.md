---
title: "<s>Zeus: Bitcoin/Lightning Wallet</s>"
altTitle: 

users: 500
appId: com.zeusln.zeus
launchDate: 2019-03-16
latestUpdate: 2020-05-10
apkVersionName: "Varies with device"
stars: 4.7
ratings: 30
reviews: 21
size: Varies with device
website: 
repository: 
issue: 
icon: com.zeusln.zeus.png
bugbounty: 
verdict: defunct # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-07-14
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.zeusln.zeus/
  - /posts/com.zeusln.zeus/
---


This app was discontinued due to release key issues. It now can be found
[here](/app.zeusln.zeus/).