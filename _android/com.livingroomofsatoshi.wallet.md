---
title: "Wallet of Satoshi"
altTitle: 

users: 10000
appId: com.livingroomofsatoshi.wallet
launchDate: 2019-05-19
latestUpdate: 2020-09-23
apkVersionName: "1.9.11"
stars: 3.8
ratings: 203
reviews: 120
size: 10M
website: http://www.walletofsatoshi.com/
repository: 
issue: 
icon: com.livingroomofsatoshi.wallet.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2019-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: walletofsatoshi
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /walletofsatoshi/
  - /com.livingroomofsatoshi.wallet/
  - /posts/2019/12/walletofsatoshi/
  - /posts/com.livingroomofsatoshi.wallet/
---


This is a custodial wallet according to their website's FAQ:

> It is a zero-configuration custodial wallet with a focus on simplicity and the
  best possible user experience. It can be downloaded using the links at
  walletofsatoshi.com

and therefore **not verifiable**.
