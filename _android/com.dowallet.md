---
title: "DoWallet: Bitcoin Wallet. A Secure Crypto Wallet."
altTitle: 

users: 50000
appId: com.dowallet
launchDate: 2019-01-01
latestUpdate: 2020-10-01
apkVersionName: "1.1.31"
stars: 4.5
ratings: 761
reviews: 403
size: 31M
website: https://www.dowallet.app/
repository: 
issue: 
icon: com.dowallet.png
bugbounty: 
verdict: nosource # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2019-11-23
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /dowallet/
  - /com.dowallet/
  - /posts/2019/11/dowallet/
  - /posts/com.dowallet/
---


This wallet sounds like non-custodial. From their description:

> ✓ Simple account creation.
> ✓ Simplified backup and recovery with a 12 word backup phrase.

And from their website:

> We take your security and privacy seriously.
Managing your own private keys is not easy. We are here to help.

Yet we cannot find any link to their source code on Google Play or their website
or doing a [search on GitHub](https://github.com/search?q="com.dowallet").

Our verdict: This wallet is **not verifiable**.
