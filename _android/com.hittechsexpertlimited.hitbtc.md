---
title: "HitBTC – Cryptocurrency Exchange & Trading BTC App"
altTitle: 

users: 50000
appId: com.hittechsexpertlimited.hitbtc
launchDate: 
latestUpdate: 2020-10-14
apkVersionName: "2.8.12"
stars: 4.1
ratings: 400
reviews: 211
size: 10M
website: https://hitbtc.com/
repository: 
issue: 
icon: com.hittechsexpertlimited.hitbtc.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: hitbtc
providerLinkedIn: 
providerFacebook: hitbtc
providerReddit: hitbtc

redirect_from:
  - /com.hittechsexpertlimited.hitbtc/
  - /posts/com.hittechsexpertlimited.hitbtc/
---


On Google Play this app claims

> **High-Level Security**
  Don’t let anybody sneak into your trade: account access is strictly via API
  keys and PIN-code. Plus, advanced encryption technology and highly protected
  cold storage.

Which means it is a custodial service and thus **not verifiable**.