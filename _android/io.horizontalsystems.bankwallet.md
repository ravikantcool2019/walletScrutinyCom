---
title: "Unstoppable - Invest In Crypto"
altTitle: 

users: 5000
appId: io.horizontalsystems.bankwallet
launchDate: 2018-12-18
latestUpdate: 2020-10-01
apkVersionName: "0.16.2"
stars: 4.6
ratings: 275
reviews: 244
size: 33M
website: https://unstoppable.money/
repository: https://github.com/horizontalsystems/unstoppable-wallet-android
issue: https://github.com/horizontalsystems/unstoppable-wallet-android/issues/2597
icon: io.horizontalsystems.bankwallet.png
bugbounty: 
verdict: reproducible # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-10-02
reviewStale: false
signer: c1899493e440489178b8748851b72cbed50c282aaa8c03ae236a4652f8c4f27b
reviewArchive:
- date: 2020-09-24
  version: "0.16.1"
  apkHash: ceb4d72b8ae2358c8779b5e0bba5e08bae08e1524eb9b6c0b2ca86889cc91adc
  gitRevision: f4ef07e88dfb7de43bdd0a7a3eadb6c46c0c3000
  verdict: reproducible
- date: 2020-07-22
  version: "0.16.0"
  apkHash: 6dc469ed865d0fe2cd855c7b227b54692877e4b09f6dc765d56fae80c20a2524
  gitRevision: ad53627c5be906793c9c868da64bf07d83b16de0
  verdict: nonverifiable
- date: 2020-07-15
  version: "0.15.0"
  apkHash: 9fad17afdb38e3ec1e38c4e88faddd479e179d2e7004722e6fb0bd440a6ea851
  gitRevision: cdff061989fc681a1fa928da59fb98a0be7d75b2
  verdict: reproducible
- date: 2020-06-14
  version: "0.14.1"
  apkHash: a45881e3ae8bba31c2fc09ecbb63ce5d200c77512f256971a12e9ab830ae719d
  gitRevision: a28d71826772bcb5d28cad48489b7f91b9c3b882
  verdict: reproducible
- date: 2020-06-07
  version: "0.14.0"
  apkHash: 9abbc4c1b7475c75437c416f5e103d4fd83625f0a7463be6aec545bff86d920d
  gitRevision: b5ebfb9fdedf9b686511645bae3e05fe13aa3d2f
  verdict: nonverifiable
- date: 2020-05-16
  version: "0.13.0"
  apkHash: 20b023949e0572af577beb0df94c6dbaf758be0d7bd323e632392c93c6640f2d
  gitRevision: f2664abad8e41ce1b3e225be0eae63d18a0cc053
  verdict: reproducible
- date: 2020-03-25
  version: "0.12.0"
  apkHash: cc616d5c4b67911e3ef65dc58f6cf045d18810826cd362a85776459607cb070c
  gitRevision: 45359b810b471200750ab0914f6c506054cf1123
  verdict: nonverifiable
- date: 2020-03-21
  version: "0.12.0"
  apkHash: cc616d5c4b67911e3ef65dc58f6cf045d18810826cd362a85776459607cb070c
  gitRevision: 65d19944379884fc3f0b9268e7e83b7dda63b5ba
  verdict: nonverifiable
- date: 2020-01-31
  version: "0.11.0"
  apkHash: f5bd6b218bb5e4fa605ed0c8e3dd9f424baf2656b9008f269d9e34697e0b21c0
  gitRevision: 43d012f4990ca6fed9d4b042224bf8fdd48ff41e
  verdict: reproducible
- date: 2020-01-29
  version: "0.11.0"
  apkHash: f5bd6b218bb5e4fa605ed0c8e3dd9f424baf2656b9008f269d9e34697e0b21c0
  gitRevision: 92e4a67ecc626220965114cd6a4cd67497e3be9f
  verdict: nonverifiable

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /io.horizontalsystems.bankwallet/
  - /posts/io.horizontalsystems.bankwallet/
---


Here is the output using our
[test script](https://gitlab.com/walletscrutiny/walletScrutinyCom/blob/master/test.sh)
on the binary from Google Play:

```
Results:
appId:          io.horizontalsystems.bankwallet
signer:         c1899493e440489178b8748851b72cbed50c282aaa8c03ae236a4652f8c4f27b
apkVersionName: 0.16.2
apkVersionCode: 32
apkHash:        6a4bb767a47990feaa3a844a9b84c0c674b7b575a9d1f9699e0a8bd4152ab4ed

Diff:
Files /tmp/fromPlay_io.horizontalsystems.bankwallet_32/apktool.yml and /tmp/fromBuild_io.horizontalsystems.bankwallet_32/apktool.yml differ
Files /tmp/fromPlay_io.horizontalsystems.bankwallet_32/original/META-INF/MANIFEST.MF and /tmp/fromBuild_io.horizontalsystems.bankwallet_32/original/META-INF/MANIFEST.MF differ
Only in /tmp/fromPlay_io.horizontalsystems.bankwallet_32/original/META-INF: RELEASEK.RSA
Only in /tmp/fromPlay_io.horizontalsystems.bankwallet_32/original/META-INF: RELEASEK.SF
```

That's how it should look like to give it the verdict: **reproducible**.
