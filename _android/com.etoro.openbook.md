---
title: "eToro"
altTitle: 

users: 5000000
appId: com.etoro.openbook
launchDate: 2013-11-05
latestUpdate: 2020-10-21
apkVersionName: "278.0.0"
stars: 4.3
ratings: 44155
reviews: 16289
size: 42M
website: 
repository: 
issue: 
icon: com.etoro.openbook.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: etoro
providerLinkedIn: company/etoro
providerFacebook: eToro
providerReddit: 

redirect_from:
  - /com.etoro.openbook/
  - /posts/com.etoro.openbook/
---


This page was created by a script from the **appId** "com.etoro.openbook" and public
information found
[here](https://play.google.com/store/apps/details?id=com.etoro.openbook).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.