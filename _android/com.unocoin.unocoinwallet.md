---
title: "Unocoin Wallet"
altTitle: 

users: 1000000
appId: com.unocoin.unocoinwallet
launchDate: 2016-11-30
latestUpdate: 2020-10-25
apkVersionName: "3.3.3"
stars: 4.6
ratings: 14022
reviews: 7538
size: 9.2M
website: https://www.unocoin.com
repository: 
issue: 
icon: com.unocoin.unocoinwallet.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: Unocoin
providerLinkedIn: company/unocoin
providerFacebook: unocoin
providerReddit: 

redirect_from:
  - /com.unocoin.unocoinwallet/
  - /posts/com.unocoin.unocoinwallet/
---


This app appears to be the interface to a trading platform. The description on
Google Play does not talk about where the keys are stored but it links to their
website and there we read

> AES-256 Encryption
> 
> The address-private key pairs obtained are encrypted using AES-256, sealed in
  envelopes and stored in multiple safe deposit lockers.

which clearly means they have the keys and you don't. As a custodial service,
this app is **not verifiable**.