---
title: "Mercury Cash"
altTitle: 

users: 10000
appId: com.adenter.mercurycash
launchDate: 
latestUpdate: 2020-09-25
apkVersionName: "4.2.5"
stars: 4.4
ratings: 153
reviews: 107
size: 82M
website: http://mercury.cash/
repository: 
issue: 
icon: com.adenter.mercurycash.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-08-06
reviewStale: true
signer: 
reviewArchive:


providerTwitter: mercurycash
providerLinkedIn: 
providerFacebook: mercurycash
providerReddit: 

redirect_from:
  - /com.adenter.mercurycash/
---


This app makes no claims about self-custody so we have to assume it is a
custodial product and thus **not verifiable**.
