---
title: "Crypto.com l DeFi Wallet"
altTitle: 

users: 50000
appId: com.defi.wallet
launchDate: 
latestUpdate: 2020-09-29
apkVersionName: "1.1.2"
stars: 4.2
ratings: 615
reviews: 250
size: 21M
website: https://www.crypto.com/
repository: 
issue: 
icon: com.defi.wallet.png
bugbounty: 
verdict: nosource # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-07-14
reviewStale: true
signer: 
reviewArchive:


providerTwitter: cryptocom
providerLinkedIn: company/cryptocom
providerFacebook: CryptoComOfficial
providerReddit: Crypto_com

redirect_from:
  - /com.defi.wallet/
  - /posts/com.defi.wallet/
---


This app's description is promising:

> Decentralized:
> - Gain full control of your crypto and private keys [...]

On their website though we find only links to
[their other app](/co.mona.android/) and no source code.

Searching their `appId` on GitHub,
[yields nothing](https://github.com/search?q=%22com.defi.wallet%22) neither.

This brings us to the verdict: **not verifiable**.