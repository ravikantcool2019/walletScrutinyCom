---
title: "Evercoin: Bitcoin, Ripple, ETH"
altTitle: 

users: 10000
appId: com.evercoin
launchDate: 
latestUpdate: 2020-09-06
apkVersionName: "2.8.7"
stars: 3.9
ratings: 180
reviews: 126
size: 40M
website: https://evercoin.com
repository: 
issue: 
icon: com.evercoin.png
bugbounty: 
verdict: nosource # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-06-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: everc0in
providerLinkedIn: 
providerFacebook: evercoin
providerReddit: 

redirect_from:
  - /com.evercoin/
  - /posts/com.evercoin/
---


This app's description says:

> Evercoin is an integrated non-custodial wallet for managing and exchanging
  cryptocurrencies.

So ... is there source coude to reproduce the build?

Unfortunately there is no mention of source code anywhere. Absent source code
this app is **not verifiable**.
