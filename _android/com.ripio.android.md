---
title: "Ripio Bitcoin Wallet: the new digital economy"
altTitle: 

users: 500000
appId: com.ripio.android
launchDate: 2015-06-01
latestUpdate: 2020-10-22
apkVersionName: "3.7.5"
stars: 3.6
ratings: 7664
reviews: 4025
size: 29M
website: https://www.ripio.com/ar/wallet/
repository: 
issue: 
icon: com.ripio.android.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-03-28
reviewStale: true
signer: 
reviewArchive:


providerTwitter: ripioapp
providerLinkedIn: company/ripio
providerFacebook: RipioApp
providerReddit: 

redirect_from:
  - /ripio/
  - /com.ripio.android/
  - /posts/2019/11/ripio/
  - /posts/com.ripio.android/
---


Ripio Bitcoin Wallet: the new digital economy
does not claim to be non-custodial and looks much like an interface for an
exchange. Neither can we find any source code.

Therefore: This wallet is **not verifiable**.
