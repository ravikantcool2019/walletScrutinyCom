---
title: "Sentinel"
altTitle: 

users: 10000
appId: com.samourai.sentinel
launchDate: 
latestUpdate: 2019-10-28
apkVersionName: "3.6"
stars: 4.3
ratings: 248
reviews: 123
size: 27M
website: https://www.samouraiwallet.com/sentinel
repository: https://github.com/Samourai-Wallet/sentinel-android
issue: 
icon: com.samourai.sentinel.png
bugbounty: 
verdict: nowallet # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-04-07
reviewStale: false
signer: 
reviewArchive:


providerTwitter: samouraiwallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.samourai.sentinel/
  - /posts/com.samourai.sentinel/
---


As this app has no private keys to protect, we do not consider it a wallet.