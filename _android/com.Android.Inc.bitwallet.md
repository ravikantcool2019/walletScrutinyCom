---
title: "BitWallet - Buy & Sell Bitcoin"
altTitle: 

users: 5000
appId: com.Android.Inc.bitwallet
launchDate: 
latestUpdate: 2020-10-01
apkVersionName: "1.4.9"
stars: 4.0
ratings: 351
reviews: 239
size: 26M
website: https://www.bitwallet.org/
repository: 
issue: 
icon: com.Android.Inc.bitwallet.jpg
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: bitwalletinc
providerLinkedIn: 
providerFacebook: BitWalletInc
providerReddit: 

redirect_from:
  - /com.Android.Inc.bitwallet/
  - /posts/com.Android.Inc.bitwallet/
---


This appears to be primarily an exchange and as there are no claims of you being
in sole control of your funds, we have to assume it is a custodial service and
therefore **not verifiable**.
