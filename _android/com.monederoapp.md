---
title: "Monedero: Wallet Bitcoin, Ethereum, Dash"
altTitle: 

users: 500
appId: com.monederoapp
launchDate: 
latestUpdate: 2020-09-29
apkVersionName: "1.6.1"
stars: 0.0
ratings: 
reviews: 
size: 12M
website: 
repository: 
issue: 
icon: com.monederoapp.png
bugbounty: 
verdict: fewusers # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-05-30
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.monederoapp/
  - /posts/com.monederoapp/
---


This page was created by a script from the **appId** "com.monederoapp" and public
information found
[here](https://play.google.com/store/apps/details?id=com.monederoapp).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.