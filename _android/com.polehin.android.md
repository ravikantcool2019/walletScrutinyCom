---
title: "Bitcoin Wallet - Buy BTC"
altTitle: 

users: 500000
appId: com.polehin.android
launchDate: 2019-01-01
latestUpdate: 2020-10-10
apkVersionName: "3.3.4"
stars: 4.2
ratings: 9902
reviews: 5862
size: 6.7M
website: https://polehin.com/
repository: 
issue: 
icon: com.polehin.android.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-03-30
reviewStale: true
signer: 
reviewArchive:
- date: 2020-11-22
  version: "3.1.4"
  apkHash: 
  gitRevision: 90d987f66d51671d7fb7097cd9676bcdce2a7c02
  verdict: nosource

providerTwitter: polehincom
providerLinkedIn: 
providerFacebook: polehincom
providerReddit: 

redirect_from:
  - /bitcoinwallet/
  - /com.polehin.android/
  - /posts/2019/11/bitcoinwallet/
  - /posts/com.polehin.android/
---


Their [User Agreement](https://polehin.com/legal/user-agreement)'s line:

> We may refuse to process or cancel any pending Digital Currency Transaction as
  required by law or any court or other authority to which Bitcoin Wallet is
  subject in any jurisdiction.

only makes sense in a custodial wallet. Our verdict: **not verifiable**.
