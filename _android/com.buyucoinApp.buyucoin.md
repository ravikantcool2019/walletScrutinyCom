---
title: "BuyUcoin - Popular Indian Crypto Currency Exchange"
altTitle: 

users: 5000
appId: com.buyucoinApp.buyucoin
launchDate: 
latestUpdate: 2019-02-26
apkVersionName: "2.0"
stars: 2.3
ratings: 63
reviews: 50
size: 7.2M
website: https://www.buyucoin.com/
repository: 
issue: 
icon: com.buyucoinApp.buyucoin.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-06-20
reviewStale: false
signer: 
reviewArchive:


providerTwitter: buyucoin
providerLinkedIn: company/buyucoin
providerFacebook: BuyUcoin
providerReddit: 

redirect_from:
  - /com.buyucoinApp.buyucoin/
  - /posts/com.buyucoinApp.buyucoin/
---


This app appears to be the broken interface for a broken exchange, judging by
the vast majority of user comments. It is certainly **not verifiable**.
