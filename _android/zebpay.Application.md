---
title: "Zebpay Bitcoin and Cryptocurrency Exchange"
altTitle: 

users: 1000000
appId: zebpay.Application
launchDate: 2014-12-23
latestUpdate: 2020-10-20
apkVersionName: "3.10.00"
stars: 4.0
ratings: 64174
reviews: 29723
size: 11M
website: https://www.zebpay.com/
repository: 
issue: 
icon: zebpay.Application.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: zebpay
providerLinkedIn: company/zebpay
providerFacebook: zebpay
providerReddit: 

redirect_from:
  - /zebpay.Application/
  - /posts/zebpay.Application/
---


In the description at Google Play we read:

> We use industry leading practice of maintaining the majority of customer
  cryptos offline

This app is an interface for an exchange and as such, only a window into what
you have in your account at that exchange. As a custodial wallet or bitcoin
bank it is **not verifiable**.