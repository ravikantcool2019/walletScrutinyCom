---
title: "Kraken Futures: Bitcoin & Crypto Futures Trading"
altTitle: 

users: 10000
appId: com.krakenfutures
launchDate: 
latestUpdate: 2019-11-29
apkVersionName: "4.0.13"
stars: 3.5
ratings: 50
reviews: 19
size: 5.5M
website: https://futures.kraken.com/
repository: 
issue: 
icon: com.krakenfutures.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-06-20
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.krakenfutures/
  - /posts/com.krakenfutures/
---


This is the interface for an exchange and nothing in the description hints at
non-custodial parts to it.
