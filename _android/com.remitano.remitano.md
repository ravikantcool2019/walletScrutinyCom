---
title: "Remitano - Buy & Sell Bitcoin Fast & Securely"
altTitle: 

users: 100000
appId: com.remitano.remitano
launchDate: 
latestUpdate: 2020-09-24
apkVersionName: "5.3.0"
stars: 4.6
ratings: 8914
reviews: 4175
size: 30M
website: https://remitano.com
repository: 
issue: 
icon: com.remitano.remitano.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: remitano
providerLinkedIn: company/Remitano
providerFacebook: remitano
providerReddit: 

redirect_from:
  - /com.remitano.remitano/
  - /posts/com.remitano.remitano/
---


This app is an interface to an exchange which holds your coins. On Google Play
and their website there is no claim to a non-custodial part to the app. As a
custodial app it is **not verifiable**.